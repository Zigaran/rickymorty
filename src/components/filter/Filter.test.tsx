import React from 'react';
import { render } from '@testing-library/react';
import Filter from './Filter';
import { Provider } from 'react-redux';
import generateStore from '../../redux/store';

let store = generateStore();

test('The filter must have an hamburger menu icon', () => {
  const { getByText } = render(<Provider store={store}><Filter /></Provider>);
  const linkElement = getByText(/Filter Menu/i);
  expect(linkElement).toBeInTheDocument();
});
